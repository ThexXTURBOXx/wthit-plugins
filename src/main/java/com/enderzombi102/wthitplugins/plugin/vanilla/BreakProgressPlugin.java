package com.enderzombi102.wthitplugins.plugin.vanilla;

import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import com.enderzombi102.wthitplugins.mixin.InteractionManagerMixin;
import mcp.mobius.waila.api.*;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.Identifier;
import net.minecraft.block.Block;
import net.minecraft.util.math.MathHelper;


public class BreakProgressPlugin implements IWailaPlugin, IBlockComponentProvider {
	private static final Identifier BREAK_PROGRESS_ID = Const.getId("break_progress");


	@Override
	public void register(IRegistrar registrar) {
		registrar.addConfig( BREAK_PROGRESS_ID, true );
		registrar.addComponent( this, TooltipPosition.BODY, Block.class );
	}

	@Override
	public void appendBody(ITooltip tooltip, IBlockAccessor accessor, IPluginConfig config) {
		if ( config.getBoolean(BREAK_PROGRESS_ID) && MinecraftClient.getInstance().interactionManager != null) {
			final int progress = MathHelper.floor(
					(
							(InteractionManagerMixin) MinecraftClient.getInstance().interactionManager
					).getCurrentBreakingProgress() * 100f
			);

			if (progress > 0) {
				tooltip.add( Util.getFullText("progression", progress) );
			}
		}
	}
}
