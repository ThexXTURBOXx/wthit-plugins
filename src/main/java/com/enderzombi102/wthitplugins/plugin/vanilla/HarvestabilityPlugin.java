package com.enderzombi102.wthitplugins.plugin.vanilla;

import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import mcp.mobius.waila.api.*;
import net.fabricmc.fabric.api.tool.attribute.v1.ToolManager;
import net.minecraft.util.Identifier;
import net.minecraft.block.Block;

public class HarvestabilityPlugin implements IWailaPlugin, IBlockComponentProvider {
	private static final Identifier HARVESTABILITY_ID = Const.getId("harvestability");
	private static final Identifier HARDNESS_ID = Const.getId("hardness");

	@Override
	public void register(IRegistrar registrar) {
		registrar.addConfig( HARVESTABILITY_ID, true );
		registrar.addConfig( HARDNESS_ID, true );
		registrar.addComponent( this, TooltipPosition.BODY, Block.class, 2000 );
	}

	@Override
	public void appendBody(ITooltip tooltip, IBlockAccessor accessor, IPluginConfig config) {
		if ( config.getBoolean( HARVESTABILITY_ID ) ) {
			if ( accessor.getBlockState().getHardness( accessor.getWorld(), accessor.getPosition() ) < 0 ) {
				tooltip.add( Util.getFullText( "unbreakable") );
			} else if ( isHarvestable( accessor ) ) {
				tooltip.add( Util.getFullText( "harvestable") );
			} else {
				tooltip.add( Util.getFullText( "not_harvestable" ) );
			}
		}
		if ( config.getBoolean( HARDNESS_ID ) ) {
			tooltip.add(
					Util.getFullText(
							"hardness",
							accessor.getBlockState().getHardness(
									accessor.getWorld(),
									accessor.getPosition()
							)
					)
			);
		}
	}

	public boolean isHarvestable(IBlockAccessor accessor) {
		return !accessor.getBlockState().isToolRequired() || ToolManager.handleIsEffectiveOn( accessor.getBlockState(),
				accessor.getPlayer().getMainHandStack(), accessor.getPlayer() );
	}

}
