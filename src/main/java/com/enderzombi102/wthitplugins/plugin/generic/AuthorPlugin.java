package com.enderzombi102.wthitplugins.plugin.generic;

import com.enderzombi102.wthitplugins.ClientWthitPlugins;
import com.enderzombi102.wthitplugins.Const;
import com.enderzombi102.wthitplugins.Util;
import com.enderzombi102.wthitplugins.mixin.KeyBindingAccessor;
import mcp.mobius.waila.api.*;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.metadata.Person;
import net.minecraft.block.Block;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.util.InputUtil;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Optional;

public class AuthorPlugin implements IWailaPlugin, IEntityComponentProvider, IBlockComponentProvider, IEventListener {
	private static final Identifier AUTHOR_ID = Const.getId("author");
	private static final HashMap< String, String > MOD_AUTHORS = new HashMap<>() {{
		put("minecraft", "mojang");
	}};

	@Override
	public void register(IRegistrar registrar) {
		registrar.addConfig( AUTHOR_ID, true );
		registrar.addComponent( (IEntityComponentProvider) this, TooltipPosition.TAIL, LivingEntity.class );
		registrar.addComponent( (IBlockComponentProvider) this, TooltipPosition.TAIL, Block.class );
		registrar.addEventListener( this );
	}

	@Override
	public void appendTail(ITooltip tooltip, IBlockAccessor accessor, IPluginConfig config) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			tooltip.add(
					Util.getFullText(
							"madeby",
							getAuthors(
									Registry.BLOCK.getId( accessor.getBlock() ).getNamespace()
							)
					).formatted( Formatting.BLUE )
			);
		}
	}

	@Override
	public void appendTail(ITooltip tooltip, IEntityAccessor accessor, IPluginConfig config) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			tooltip.add(
					Util.getFullText(
							"madeby",
							getAuthors(
									Registry.ENTITY_TYPE.getId( accessor.getEntity().getType() ).getNamespace()
							)
					).formatted( Formatting.BLUE )
			);
		}
	}

	@Override
	public @Nullable String getHoveredItemModName(ItemStack stack, IPluginConfig config) {
		if ( config.getBoolean( AUTHOR_ID ) && shouldDisplay() ) {
			return getAuthors( Registry.ITEM.getId( stack.getItem() ).getNamespace() );
		}
		return null;
	}

	private static boolean shouldDisplay() {
		return InputUtil.isKeyPressed(
				MinecraftClient.getInstance().getWindow().getHandle(),
				( (KeyBindingAccessor) ClientWthitPlugins.getInstance().showAuthorsBinding ).getBoundKey().getCode()
		);
	}

	private static @Nullable String getAuthors(String modid) {
		MOD_AUTHORS.computeIfAbsent( modid, key -> {
			Optional<ModContainer> container = FabricLoader.getInstance().getModContainer( modid );
			if ( container.isPresent() ) {
				String buf = "";
				for ( Person person : container.get().getMetadata().getAuthors() )
					buf += " " + person.getName() + ",";
				return buf.substring( 0, buf.length() - 1 );
			}
			return null;
		});
		return MOD_AUTHORS.get(modid);
	}
}
