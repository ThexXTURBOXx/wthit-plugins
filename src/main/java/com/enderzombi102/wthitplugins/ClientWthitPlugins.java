package com.enderzombi102.wthitplugins;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import org.lwjgl.glfw.GLFW;

public class ClientWthitPlugins implements ClientModInitializer {
	private static ClientWthitPlugins INSTANCE;
	public KeyBinding showAuthorsBinding;

	public ClientWthitPlugins() {
		INSTANCE = this;
	}

	@Override
	public void onInitializeClient() {
		showAuthorsBinding = KeyBindingHelper.registerKeyBinding(
				new KeyBinding(
						"key.wthit-plugins.show_author",
						GLFW.GLFW_KEY_LEFT_SHIFT,
						"WTHIT"
				)
		);
	}

	public static ClientWthitPlugins getInstance() {
		return INSTANCE;
	}
}
